import java.util.*;

/**
 * This class contains the methods for tokenizing and parsing the
 * arithmetic expression to postfix
 */
public class ExpressionParser {
    private static Map<Character, Integer> operatorPriorities = Map.of(
            '=', 0,
            '+', 1, '-', 1,
            '*', 2, '/', 2,
            '^', 3
    );

    List<String> convertToPostfix(String expression) throws Exception {
        List<String> tokens = tokenize(expression);
        List<String> result = new ArrayList<>();
        // used for maintaining operators priorities and braces in expression
        Deque<Character> st = new ArrayDeque<>();

        char sign = '+';
        for(int i = 0; i < tokens.size(); i++) {
            String token = tokens.get(i);
            String prevToken = (i - 1 < 0) ? "" : tokens.get(i - 1);
            String nextToken = (i + 1 == tokens.size()) ? "" : tokens.get(i + 1);

            if(isIdentifierOrNumber(token)) {
                if(isMisplacedIdentifierOrNumber(token, prevToken)) {
                    throw new Exception("Invalid expression - misplaced number or identifier");
                }
                result.add((sign == '-' ? '-' : "") + token);
                sign = '+';
            } else if(isOperator(token)) {
                char op = token.charAt(0);
                if(isUnarySign(op, prevToken, nextToken)) {
                    sign = op;
                } else if(isMisplacedOperator(op, prevToken, nextToken)) {
                    throw new Exception("Invalid expression - misplaced operator");
                } else {
                    while(!st.isEmpty() && st.peekFirst() != '(' && higherPriority(st.peekFirst(), op)) {
                        result.add(String.valueOf(st.poll()));
                    }

                    st.offerFirst(op);
                }
            } else if(token.charAt(0) == '(') {
                if(i != 0 && st.isEmpty()) {
                    throw new Exception("Invalid expression - misplaced open brace");
                }
                st.push('(');
            }  else if(token.charAt(0) == ')') {
                while(!st.isEmpty() && st.peekFirst() != '(') {
                    result.add(String.valueOf(st.pop()));
                }

                if(!st.isEmpty() && st.peekFirst() == '(') {
                    st.pop();
                } else {
                    throw new Exception("Invalid expression - unbalanced braces");
                }
            } else {
                throw new Exception("Invalid expression - illegal character");
            }
        }

        while(!st.isEmpty()) {
            char c = st.poll();
            if(c == '(') {
                throw new Exception("Invalid expression - open braces left unclosed");
            }
            result.add(String.valueOf(c));
        }

        return result;
    }

    List<String> tokenize(String expression) {
        List<String> result = new ArrayList<>();
        StringBuilder currToken = new StringBuilder();

        int i = 0;
        while(i < expression.length()) {

            while(i < expression.length() && isWhiteSpace(expression.charAt(i))) {
                i++;
            }

            if(Character.isLetterOrDigit(expression.charAt(i))) {
                while(i < expression.length() && Character.isLetterOrDigit(expression.charAt(i))) {
                    currToken.append(expression.charAt(i));
                    i++;
                }
            } else if(isPlusOrMinus(expression.charAt(i))) {
                while(i < expression.length() && (isPlusOrMinus(expression.charAt(i)) || isWhiteSpace(expression.charAt(i)))) {
                    if(isPlusOrMinus(expression.charAt(i))) {
                        currToken.append(expression.charAt(i));
                    }
                    i++;
                }
            } else {
                currToken.append(expression.charAt(i));
                i++;
            }

            if(currToken.length() > 0) {
                result.add(substituteOperator(currToken.toString()));
                currToken.setLength(0);
            }
        }

        return result;
    }

    public static boolean isOperator(String t) {
        return operatorPriorities.containsKey(t.charAt(0)) && t.length() == 1;
    }

    public static boolean isNumber(String t) {
        return t.matches("-?\\d+");
    }

    public static boolean isInvalidIdentifier(String t) {
        return !t.matches("-?[a-zA-Z]+");
    }

    public static boolean isIdentifierOrNumber(String t) {
        return t.matches("-?\\w+");
    }

    private static boolean isPlusOrMinus(char op) {
        return op == '+' || op == '-';
    }

    private static boolean isWhiteSpace(char c) {
        return Character.isWhitespace(c) || Character.isSpaceChar(c);
    }

    private static boolean isMisplacedIdentifierOrNumber(String token, String prevToken) {
        return Objects.equals(prevToken, ")") || isIdentifierOrNumber(prevToken);
    }

    private static boolean isUnarySign(char op, String prevToken, String nextToken) {
        return isPlusOrMinus(op) && isIdentifierOrNumber(nextToken) &&
                (Objects.equals(prevToken, "") || Objects.equals(prevToken, "(") || isOperator(prevToken));
    }

    private static boolean isMisplacedOperator(char op, String prevToken, String nextToken) {
        return Objects.equals(prevToken, "") || Objects.equals(prevToken, "(") || isOperator(prevToken)
                || Objects.equals(nextToken, "") || Objects.equals(nextToken, ")");
    }

    private static boolean higherPriority(char op1, char op2) {
        return operatorPriorities.get(op1) >= operatorPriorities.get(op2);
    }

    private static String substituteOperator(String t) {
        if(t.matches("[+\\-]+")) {
            int countMinus = 0;
            for(char ch : t.toCharArray()) {
                if(ch == '-') {
                    countMinus++;
                }
            }

            return countMinus % 2 == 0 ? "+" : "-";
        } else {
            return t;
        }
    }
}
