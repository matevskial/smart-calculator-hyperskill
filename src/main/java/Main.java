import java.util.Scanner;

/**
 * The main entry of the program, it initializes the Calculator
 * and waits  for user input.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Calculator c = new Calculator();

        while(!c.isClosed()) {
            String line = scanner.nextLine();
            c.execute(line);
        }
    }
}
