# Smart Calculator

A project from HyperSkill Java developer curriculum ([hyperskill.org](hyperskill.org))

It is a console application that reads arithmetic expressions, evaluates them
and print the result

![Smart Calculator in action gif](media/smart-calculator-in-action.gif "Smart Calculator in action")

# Features

* Arbitrary-length integers(no decimal numbers)

* Variables

* Supported operations are addition, subtraction, mutliplication and exponentiation