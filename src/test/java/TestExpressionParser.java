import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TestExpressionParser {
    @Test
    public void testTokenizer() {
        ExpressionParser e = new ExpressionParser();

        String[] expressions = {
                "2 + 3",
                "1 +- 3- -+ 5",
                "+18",
                "-18",
                "+- 18",
                "1 + 2 +",
                "21++",
                "9 6",
                "2 *- 4",
                "4 *+/- 6",
                "((1 + 2))",
                "12 *-* 6",
                "3 + 8 * ((4 + 3) * 2 + 1) - 6 / (2 + 1)"
        };
        String[][] expects = new String[][] {
                {"2", "+", "3"},
                {"1", "-", "3", "+", "5"},
                {"+", "18"},
                {"-", "18"},
                {"-", "18"},
                {"1", "+", "2", "+"},
                {"21", "+"},
                {"9", "6"},
                {"2", "*", "-", "4"},
                {"4", "*", "+", "/", "-", "6"},
                {"(", "(", "1", "+", "2", ")", ")"},
                {"12", "*", "-", "*", "6"},
                {"3", "+", "8", "*", "(", "(", "4", "+", "3", ")", "*", "2", "+", "1", ")", "-", "6", "/", "(", "2", "+", "1", ")"}
        };

        for(int i = 0; i < expressions.length; i++) {
            String expr = expressions[i];
            String[] expected = expects[i];

            String[] actual = e.tokenize(expr).toArray(new String[0]);

            String failMessage = String.format("idx: %d, expression: %s, expected: %s, actual: %s", i, expr, Arrays.asList(expected), Arrays.asList(actual));

            assertArrayEquals(failMessage, expected, actual);
        }
    }

    @Test
    public void testValidConversionToPostfix() throws Exception {
        ExpressionParser e = new ExpressionParser();

        String[] expressions = {
                "2 + 3",
                "1 +- 3- -+ 5",
                "+18",
                "-18",
                "+- 18",
                "2 *- 4",
                "((1 + 2))",
                "((1+2)+3)*2-8/4"
        };

        String[][] expects = new String[][]{
                {"2", "3", "+"},
                {"1", "3", "-", "5", "+"},
                {"18"},
                {"-18"},
                {"-18"},
                {"2", "-4", "*"},
                {"1", "2", "+"},
                {"1", "2", "+", "3", "+", "2", "*", "8", "4", "/", "-"}
        };

        for(int i = 0; i < expressions.length; i++) {
            String expr = expressions[i];
            String[] expected = expects[i];

            String[] actual = e.convertToPostfix(expr).toArray(new String[0]);

            String failMessage = String.format("idx: %d, expression: %s, expected: %s, actual: %s", i, expr, Arrays.asList(expected), Arrays.asList(actual));

            assertArrayEquals(failMessage, expected, actual);
        }
    }

    @Test()
    public void testInvalidConversionsToPostfix() throws Exception {
        ExpressionParser e = new ExpressionParser();

        String[] expressions = {
                "1 + 2 + ",
                "21++",
                "9 6",
                "4 *+/- 6",
                "12 *-* 6",
                "*6",
                "*+-"
        };

        for(String expr : expressions) {
            assertEquals(expr,"ExceptionThrown", testInvalidConversionToPostfix(e, expr));
        }
    }

    private String testInvalidConversionToPostfix(ExpressionParser e, String expr) {
        try {
            e.convertToPostfix(expr);
            return "NoError";
        } catch (Exception ex) {
            return "ExceptionThrown";
        }
    }
}
