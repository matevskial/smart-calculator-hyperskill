import java.math.BigInteger;
import java.util.*;

/**
 * This class represents a calculator that executes commands prefixed with /
 * or evaluates arithmetic expressions that are parsed with ExpressionParser
 */
public class Calculator {
    private boolean closed;

    private Map<String, BigInteger> variables;
    private ExpressionParser expressionParser;

    public Calculator() {
        this.closed = false;
        variables = new HashMap<>();
        expressionParser = new ExpressionParser();
    }

    public void execute(String input) {
        input = input.trim();
        if("".equals(input) || input.startsWith("/")) {
            executeCommand(input);
        } else {
            try {
                evaluate(input);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }

    public boolean isClosed() {
        return closed;
    }

    private void executeCommand(String command) {
        switch (command) {
            case "/help":
                System.out.println("The program calculates the result of arithmetic expressions.");
                break;
            case "/exit":
                System.out.println("Bye!");
                closed = true;
                break;
            case "":
                break;
            default:
                System.out.println("Unknown command");
        }
    }

    private void evaluate(String expression) throws Exception {
        List<String> postfix = expressionParser.convertToPostfix(expression);

        if(postfix.size() >= 3 && "=".equals(postfix.get(2))) {
            makeAssignment(postfix);
            return;
        }

        // used for evaluating the expression given in postfix
        Deque<BigInteger> st = new ArrayDeque<>();
        for(int i = 0; i < postfix.size(); i++) {
            String t = postfix.get(i);
            if(ExpressionParser.isOperator(t)) {
                BigInteger arg2 = st.pollFirst();
                BigInteger arg1 = st.pollFirst();

                st.offerFirst(doOperation(arg1, arg2, t.charAt(0)));
            } else if(ExpressionParser.isNumber(t)) {
                st.push(new BigInteger(t));
            } else {
                if(ExpressionParser.isInvalidIdentifier(t)) {
                    throw new Exception("Invalid identifier");
                }

                BigInteger value = getValue(t);
                st.offerFirst(value);
            }
        }

        System.out.println(st.peekFirst());
    }

    private void makeAssignment(List<String> postfix) throws Exception {
        if(postfix.size() > 3) {
            throw new Exception("Invalid assignment");
        }

        String variable = postfix.get(0);
        String value = postfix.get(1);

        if(ExpressionParser.isInvalidIdentifier(variable)) {
            throw new Exception("Invalid identifier");
        }

        if(ExpressionParser.isNumber(value)) {
            variables.put(variable, new BigInteger(value));
        } else {
            if(ExpressionParser.isInvalidIdentifier(value)) {
                throw new Exception("Invalid assignment");
            }

            BigInteger v = getValue(value);
            variables.put(variable, v);
        }
    }
    
    private BigInteger getValue(String identifier) throws Exception {
        int sign = 1;
        if(identifier.startsWith("-")) {
            sign  = -1;
            identifier = identifier.substring(1);
        }

        if(!variables.containsKey(identifier)) {
            throw new Exception("Unknown variable");
        } else {

            return variables.get(identifier).multiply(BigInteger.valueOf(sign));
        }
    }

    private BigInteger doOperation(BigInteger arg1, BigInteger arg2, char op) {
        switch (op) {
            case '+':
                arg1 = arg1.add(arg2);
                break;
            case '-':
                arg1 = arg1.subtract(arg2);
                break;
            case '*':
                arg1 = arg1.multiply(arg2);
                break;
            case '/':
                arg1 = arg1.divide(arg2);
                break;
            case '^':
                arg1 = arg1.pow(arg2.intValue());
                break;
            default:
                System.out.println("Unknown operator");
        }
        return arg1;
    }
}
